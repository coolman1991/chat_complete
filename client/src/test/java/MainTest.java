import net.thumbtack.Main;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by olll on 11.12.2015.
 */
public class MainTest {

    @Test
    public void sumTest() {
        Main main = new Main();
        Assert.assertEquals(15, main.sum(10, 5));
    }
}