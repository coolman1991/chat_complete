package net.thumbtack.action;

/**
 * Created by olzh0813 on 30.07.2015.
 */
public class ActionException extends Exception {

    public ActionException(String message) {
        super(message);
    }

    public ActionException(Throwable cause) {
        super(cause);
    }
}
