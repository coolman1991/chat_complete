package net.thumbtack.action;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.cookie.Cookie;
import io.netty.handler.codec.http.cookie.DefaultCookie;
import io.netty.handler.codec.http.cookie.ServerCookieDecoder;
import io.netty.handler.codec.http.cookie.ServerCookieEncoder;
import net.thumbtack.chat.ChatOrchestrator;
import net.thumbtack.chat.model.User;
import org.apache.commons.lang.StringUtils;

import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

/**
 * Created by olzh0813 on 28.07.2015.
 */
public abstract class Action {

    protected static final int UID_SALT = 175129;
    protected static final String HEADER_COOKIE = "Cookie";
    protected static final String COOKIE_NAME = "uid";
    protected static final int COOKIE_AGE = 24 * 60 * 60;
    protected static final Random rnd = new Random();

    protected User getUser(Map<String, String> headers) {
        String uid = this.parseUidCookie(headers.get(HEADER_COOKIE));
        if(StringUtils.isBlank(uid)) {
            int id = this.createUserId();
            uid = hashUserId(id);
            User user = new User(id);
            ChatOrchestrator.getInstance().newUser(uid, user);
            return user;
        }
        return ChatOrchestrator.getInstance().getUser(uid);
    }

    private String parseUidCookie(String cookieString) {
        if(StringUtils.isBlank(cookieString)) {
            return null;
        }
        Set<Cookie> cookies = ServerCookieDecoder.STRICT.decode(cookieString);
        Optional<Cookie> optional = cookies.stream()
                .filter(cookie -> COOKIE_NAME.equals(cookie.name()))
                .findFirst();
        return optional.isPresent() ? optional.get().name() : null;
    }

    private int createUserId() {
        return rnd.nextInt(Integer.MAX_VALUE);
    }

    private String hashUserId(int id) {
        return String.valueOf(id % UID_SALT);
    }

    protected void setCookie(HttpResponse response, int id) {
        String uid = hashUserId(id);
        Cookie cookie = new DefaultCookie(COOKIE_NAME, uid);
        cookie.setMaxAge(COOKIE_AGE);
        response.headers().add(HttpHeaders.Names.SET_COOKIE, ServerCookieEncoder.STRICT.encode(cookie));
    }

    public abstract void process(ChannelHandlerContext ctx, Map<String, String> headers, Map<String, String> params)
            throws ActionException;
}
