package net.thumbtack.action.impl;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import net.thumbtack.action.Action;
import net.thumbtack.action.ActionException;
import net.thumbtack.chat.model.User;
import net.thumbtack.http.HttpUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

/**
 * Created by oleg on 14.12.15.
 */
public class SetNameAction extends Action {

    private static final String PARAM_USER_NAME = "usr_name";

    public void process(ChannelHandlerContext ctx, Map<String, String> headers, Map<String, String> params) throws ActionException {

        String name = headers.get(PARAM_USER_NAME);

        if(StringUtils.isBlank(name)) {
            HttpUtils.send400Error(ctx);
        }

        User user = getUser(headers);

        user.setName(name);

        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);

        setCookie(response, user.getId());
        ctx.write(response);
        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
    }

}
