package net.thumbtack.action.impl;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.CharsetUtil;
import net.thumbtack.action.Action;
import net.thumbtack.action.ActionException;

import java.util.Map;

/**
 * Created by olzh0813 on 27.07.2015.
 */

public class TestAction extends Action {

    private static final String CONTENT = "The body of HTTP response";

    @Override
    public void process(ChannelHandlerContext ctx, Map<String, String> headers, Map<String, String> params) throws ActionException {
        FullHttpResponse response = new DefaultFullHttpResponse(
                HttpVersion.HTTP_1_1,
                HttpResponseStatus.OK,
                Unpooled.copiedBuffer(CONTENT, CharsetUtil.UTF_8));

        ctx.write(response);
        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
    }
}