package net.thumbtack.action.impl;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.util.CharsetUtil;
import net.thumbtack.action.Action;
import net.thumbtack.action.ActionException;
import net.thumbtack.chat.ChatOrchestrator;
import net.thumbtack.chat.model.Message;
import net.thumbtack.chat.model.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * Created by oleg on 14.12.15.
 */
public class GetMessages extends Action {

    private static final String JSON_SENDER_NAME = "senderName";
    private static final String JSON_TEXT = "text";
    private static final String JSON_TIME = "time";

    private static final SimpleDateFormat SDF = new SimpleDateFormat("HH:mm:ss");

    @Override
    public void process(ChannelHandlerContext ctx, Map<String, String> headers, Map<String, String> params) throws ActionException {
        List<Message> messages = ChatOrchestrator.getInstance().getMessages();

        JSONArray jsonMessages = new JSONArray();
        messages.stream().forEach(message -> {
            JSONObject jsonMessage = new JSONObject();
            jsonMessage.put(JSON_SENDER_NAME, message.getSenderName());
            jsonMessage.put(JSON_TEXT, message.getText());
            jsonMessage.put(JSON_TIME, SDF.format(message.getTime()));
            jsonMessages.add(jsonMessage);
        });

        User user = getUser(headers);

        FullHttpResponse response = new DefaultFullHttpResponse(
                HttpVersion.HTTP_1_1,
                HttpResponseStatus.OK,
                Unpooled.copiedBuffer(jsonMessages.toJSONString(), CharsetUtil.UTF_8));

        setCookie(response, user.getId());
        ctx.write(response);
        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
    }
}
