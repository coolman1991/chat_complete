package net.thumbtack.action.impl;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import net.thumbtack.action.Action;
import net.thumbtack.action.ActionException;
import net.thumbtack.chat.ChatOrchestrator;
import net.thumbtack.chat.model.Message;
import net.thumbtack.chat.model.User;
import net.thumbtack.http.HttpUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

/**
 * Created by olzh0813 on 28.07.2015.
 */
public class SendMessageAction extends Action {
    private static final String PARAM_MESSAGE_TEXT = "msg_text";

    //private static final Logger LOG = LoggerFactory.getLogger("ad-server");

    @Override
    public void process(final ChannelHandlerContext ctx, Map<String, String> headers, Map<String, String> params) throws ActionException {

        String text = headers.get(PARAM_MESSAGE_TEXT);

        if(StringUtils.isBlank(text)) {
            HttpUtils.send400Error(ctx);
            return;
        }

        User user = getUser(headers);

        Message message = new Message(user.getName(), text);

        ChatOrchestrator.getInstance().newMessage(message);

        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);

        setCookie(response, user.getId());
        ctx.write(response);
        ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
    }
}
