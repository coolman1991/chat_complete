package net.thumbtack.action;


import net.thumbtack.action.impl.SendMessageAction;
import net.thumbtack.action.impl.TestAction;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by olzh0813 on 28.07.2015.
 */
public class ActionFactory {
    private static class SingletonHolder {
        private static final ActionFactory INSTANCE = new ActionFactory();
    }

    public static ActionFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private Map<String, Action> actions = new ConcurrentHashMap();

    ActionFactory() {
        actions.put("/test", new TestAction());
        actions.put("/get", new SendMessageAction());
    }

    public Action getAction(String uri) {
        return actions.get(uri);
    }
}
