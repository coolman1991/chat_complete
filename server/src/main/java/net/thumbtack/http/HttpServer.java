package net.thumbtack.http;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

import java.net.InetSocketAddress;

/**
 * An HTTP server that sends back json response
 */
public final class HttpServer {

    //private static final Logger LOG = LoggerFactory.getLogger("ad-server");

    private static class SingletonHolder {
        private static final HttpServer INSTANCE = new HttpServer();
    }

    public static HttpServer getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private int port;

    public void setPort(int port) {
        this.port = port;
    }

    public void start() {
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new HttpServerInitializer());

            Channel ch = b.bind(port).sync().channel();

            InetSocketAddress address = (InetSocketAddress) ch.localAddress();

            //LOG.info("AdServer started at " + address.getHostName() + ":" + port);
            System.out.println("AdServer started at " + address.getHostName() + ":" + port);

            ch.closeFuture().sync();
        } catch (InterruptedException e) {
            //LOG.error("Server error", e);
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}