package net.thumbtack.http;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.QueryStringDecoder;
import net.thumbtack.action.Action;
import net.thumbtack.action.ActionFactory;

import java.util.HashMap;
import java.util.Map;

public class HttpServerHandler extends SimpleChannelInboundHandler<Object> {

    //private static final Logger LOG = LoggerFactory.getLogger("ad-server"); //TODO REVU: We are using logback. Remove log4j.properties. See ssp-test-wrapper

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Object msg) {
        if (msg instanceof FullHttpRequest) { //TODO REVU Change it with according to the HttpObjectAggregator
            FullHttpRequest request = (FullHttpRequest) msg;
            Map<String, String> headers = new HashMap();
            Map<String, String> params = new HashMap();

            QueryStringDecoder queryStringDecoder = new QueryStringDecoder(request.getUri());
            Action action = ActionFactory.getInstance().getAction(queryStringDecoder.path());
            if(action == null) {
                System.err.println("Not found action on route " + queryStringDecoder.path());
                //LOG.error("Not found action on route " + queryStringDecoder.path());
                HttpUtils.send404Error(ctx);
                return;
            }

            request.headers().forEach(param ->
                    headers.put(param.getKey(), param.getValue()));
            queryStringDecoder.parameters().forEach((key, value) ->
                    params.put(key, value.get(0)));

            try {
                action.process(ctx, headers, params); //TODO REVU: May be it better to pass Params as Map<String, List<String>> ?
            } catch(Exception e) {
                e.printStackTrace();
                //LOG.error("Unhandled exception while processing action " + action.getClass() + " with params:" + params, e);
                HttpUtils.send204Error(ctx);
            }
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        //LOG.error("Error ", cause);
        ctx.close();
    }
}