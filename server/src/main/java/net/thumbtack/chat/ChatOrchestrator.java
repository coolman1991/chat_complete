package net.thumbtack.chat;

import net.thumbtack.chat.model.Message;
import net.thumbtack.chat.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by oleg on 14.12.15.
 */
public class ChatOrchestrator {

    private static class SingletonHolder {
        private static final ChatOrchestrator INSTANCE = new ChatOrchestrator();
    }

    public static ChatOrchestrator getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private Map<String, User> users;
    private Queue<Message> messages;

    private ChatOrchestrator() {
        users = new ConcurrentHashMap();
        messages = new ConcurrentLinkedQueue();
    }

    public User getUser(String uid) {
        return users.get(uid);
    }

    public void newUser(String uid, User user) {
        users.put(uid, user);
    }

    public void newMessage(Message message) {
        if(messages.size() > 10) {
            messages.poll();
        }
        messages.offer(message);
    }

    public List<Message> getMessages() {
        return new ArrayList(messages);
    }
}
