package net.thumbtack.chat.model;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by oleg on 14.12.15.
 */
public class Message {
    private String senderName;
    private String text;
    private Date time;

    public Message(String senderName, String text) {
        this.senderName = senderName;
        this.text = text;
        this.time = Calendar.getInstance().getTime();
    }

    public String getSenderName() {
        return senderName;
    }

    public String getText() {
        return text;
    }

    public Date getTime() {
        return time;
    }
}
