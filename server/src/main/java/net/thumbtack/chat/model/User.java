package net.thumbtack.chat.model;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by oleg on 14.12.15.
 */
public class User {

    private int id;
    private String name;
    private Date connectionTime;

    public User(int id) {
        this.id = id;
        this.name = "Anonymous-" + id;
        this.connectionTime = Calendar.getInstance().getTime();
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
