package net.thumbtack.config;

/**
 * Configuration manager exception.
 */
public class ConfigurationManagerException extends Exception {

    /**
     * Create new exception.
     *
     * @param message Message
     */
    public ConfigurationManagerException(final String message) {
        super(message);
    }

    /**
     * Create new exception.
     *
     * @param message Message.
     * @param  cause Cause.
     */
    public ConfigurationManagerException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
