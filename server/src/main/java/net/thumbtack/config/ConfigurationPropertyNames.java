package net.thumbtack.config;

/**
 * Property names.
 */
public final class ConfigurationPropertyNames {

    /**
     * String host.
     */
    public static final String HTTP_HOST = "http.host";

    /**
     * Int port.
     */
    public static final String HTTP_PORT = "http.port";

    private ConfigurationPropertyNames() {

    }
}
