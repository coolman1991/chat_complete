package net.thumbtack.config;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;

import java.io.StringReader;

/**
 * This class used for configuration managing.
 */
public final class ConfigurationManager {

    //private static final Logger LOG = LoggerFactory.getLogger(ConfigurationManager.class);

    private static final PropertiesConfiguration configuration = new PropertiesConfiguration();

    private ConfigurationManager() {

    }


    /**
     * Load configuration from file.
     *
     * @param configFileName Path to configuration file.
     * @throws ConfigurationManagerException .
     */
    public static void loadConfigFile(final String configFileName) throws ConfigurationManagerException {
        try {
            String configurationFileName = configFileName;
            if (StringUtils.isNotBlank(configurationFileName)) {

                PropertiesConfiguration customConfig = new PropertiesConfiguration(configurationFileName);
                configuration.copy(customConfig);
            } else {
                throw new ConfigurationManagerException("Empty configuration file name");
            }
        } catch (ConfigurationException e) {
            //LOG.error("Error Load configuration.", e);
            throw new ConfigurationManagerException("Error Load configuration", e);
        }

    }

    /**
     * Load configuration from body.
     *
     * @param config Body.
     * @throws ConfigurationManagerException .
     */
    public static void loadConfigBody(final String config) throws ConfigurationManagerException {
        try {
            if (StringUtils.isNotBlank(config)) {

                PropertiesConfiguration customConfig = new PropertiesConfiguration();
                StringReader reader = new StringReader(config);
                try {
                    customConfig.load(reader);
                } finally {
                    reader.close();
                }

                configuration.copy(customConfig);
            } else {
                throw new ConfigurationManagerException("Empty configuration file name");
            }
        } catch (ConfigurationException e) {
            //LOG.error("Error Load configuration.", e);
            throw new ConfigurationManagerException("Error Load configuration", e);
        }

    }

    /**
     * Get current configuration.
     *
     * @return {@link PropertiesConfiguration}
     * @throws ConfigurationManagerException .
     */
    public static PropertiesConfiguration getConfiguration() throws ConfigurationManagerException {
        if (configuration.isEmpty()) {
            throw new ConfigurationManagerException("Configuration not initialized");
        }

        return configuration;
    }
}
