import net.thumbtack.config.ConfigurationManager;
import net.thumbtack.config.ConfigurationManagerException;
import net.thumbtack.config.ConfigurationPropertyNames;
import net.thumbtack.http.HttpServer;
import org.apache.commons.lang.StringUtils;

/**
 * Created by olll on 12.12.2015.
 */

public class Main {

    //private static final Logger LOG = LoggerFactory.getLogger("ad-server");

    /**
     * Program entry point.
     * @param args Program arguments
     */
    public static void main(String[] args) {

        if(args.length < 2 || !StringUtils.equals("-configFile", args[0])) {
            //LOG.error("No configuration file specified");
            System.exit(0);
        }
        String configFile = args[1];
        try {
            ConfigurationManager.loadConfigFile(configFile);
        } catch (ConfigurationManagerException e) {
            //LOG.error("Configuration file exception: ", e);
            System.exit(0);
        }

        try {
            int port = ConfigurationManager.getConfiguration().getInt(ConfigurationPropertyNames.HTTP_PORT);
            HttpServer.getInstance().setPort(port);
            HttpServer.getInstance().start();

        } catch (Exception e) {
            //LOG.error(e.getMessage());
            System.exit(0);
        }
    }
}